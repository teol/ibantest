Test iBanFirst
=====

PHP 7.4, Symfony 5.1, Twig, amphp/http-client

# 1. Installation
1°) Copy the .env file to .env.local and fill in your API credentials (IBANFIRST_LOGIN, IBANFIRST_PASSWORD)

## 1.1 With Composer & Symfony CLI Installer (& php7.4+)
2°) In the project's root directory:

``composer install``

``symfony server:start -d``

## 1.2 With docker-compose (Docker Engine 18.06.0+)
2°) In the project's root directory:

``make install`` (will build docker images, start the containers and install composer dependencies)

Run it as root (sudo) on Linux.

# 2. Website
3°) Visit the website: ``http://127.0.0.1:8000``

4°) Enjoy :man_shrugging:



# 3. Stop

## 3.1 With Symfony CLI Installer

``symfony server:stop``

## 3.2 With docker-compose

``make stop``