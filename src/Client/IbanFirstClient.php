<?php

declare(strict_types=1);

namespace App\Client;

use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use function Safe\json_decode;
use function Safe\sprintf;

class IbanFirstClient
{
    protected HttpClientInterface $httpClient;
    protected LoggerInterface $logger;
    protected string $ibanFirstLogin;
    protected string $ibanFirstPassword;
    protected string $apiBaseUrl;

    public function __construct(
        HttpClientInterface $httpClient,
        LoggerInterface $logger,
        string $ibanFirstLogin,
        string $ibanFirstPassword,
        string $apiBaseUrl
    ) {
        $this->httpClient         = $httpClient;
        $this->logger             = $logger;
        $this->ibanFirstLogin     = $ibanFirstLogin;
        $this->ibanFirstPassword  = $ibanFirstPassword;
        $this->apiBaseUrl         = $apiBaseUrl;
    }

    public function requestListWallets(
        ?int $page = null,
        ?int $perPage = null,
        ?string $sort = null
    ): array {
        $queryParams = [];
        if (null !== $page) {
            $queryParams['page'] = $page;
        }
        if (null !== $perPage) {
            $queryParams['perPage'] = $perPage;
        }
        if (null !== $sort) {
            $queryParams['sort'] = $sort;
        }

        $content = $this->get('/wallets/');

        try {
            if (!empty($content)) {
                return json_decode($content, true);
            }
        } catch (\Throwable $e) {
            $this->logger->error('Error during wallet list request: '.$e->getMessage());
        }

        return [];
    }

    public function requestWalletDetails(string $walletId): array
    {
        $content = $this->get('/wallets/-'.$walletId);

        try {
            if (!empty($content)) {
                return json_decode($content, true);
            }
        } catch (\Throwable $e) {
            $this->logger->error('Error during wallet details request: '.$e->getMessage());
        }

        return [];
    }

    public function requestWalletMovements(
        string $walletId,
        ?\DateTime $fromDate = null,
        ?\DateTime $toDate = null,
        ?int $page = null,
        ?int $perPage = null,
        ?string $sort = null
    ): array {
        $queryParams = ['id' => $walletId];
        if (null !== $fromDate) {
            $queryParams['fromDate'] = $fromDate->format('Y-m-d H:i:s');
        }
        if (null !== $toDate) {
            $queryParams['toDate'] = $fromDate->format('Y-m-d H:i:s');
        }
        if (null !== $page) {
            $queryParams['page'] = $page;
        }
        if (null !== $perPage) {
            $queryParams['perPage'] = $perPage;
        }
        if (null !== $sort) {
            $queryParams['sort'] = $sort;
        }

        $content = $this->get('/financialMovements/', $queryParams);

        try {
            if (!empty($content)) {
                return json_decode($content, true);
            }
        } catch (\Throwable $e) {
            $this->logger->error('Error during wallet movements request: '.$e->getMessage());
        }

        return [];
    }

    public function requestMovementDetails(string $movementId): array
    {
        $content = $this->get('/financialMovements/-'.$movementId);

        try {
            if (!empty($content)) {
                return json_decode($content, true);
            }
        } catch (\Throwable $e) {
            $this->logger->error('Error during movement details request: '.$e->getMessage());
        }

        return [];
    }

    protected function get(string $uri, ?array $queryParams = null): ?string
    {
        $options = ['headers' => $this->getDefaultHeaders()];
        if (!empty($queryParams)) {
            $options['query'] = $queryParams;
        }

        try {
            $response = $this->httpClient->request('GET', $this->apiBaseUrl.$uri, $options);
            if (200 === $response->getStatusCode()) {
                return $response->getContent();
            }
        } catch (\Throwable $e) {
            $this->logger->error('Error during GET Request: '.$e->getMessage(), [
                'response' => $response
            ]);
        }

        return null;
    }

    protected function getDefaultHeaders(): array
    {
        return [
            'Accept' => 'application/json',
            'X-WSSE' => $this->computeAuthenticationHeader()
        ];
    }

    protected function computeAuthenticationHeader(): string
    {
        try {
            $nonce = \random_bytes(16);
            $nonce64 = \base64_encode($nonce);
            $date = new \DateTime('now', new \DateTimeZone('UTC'));
            $dateString = $date->format('Y-m-d\TH:i:s\Z');

            $digest = \base64_encode(\sha1($nonce.$dateString.$this->ibanFirstPassword, true));

            $wsseHeader = sprintf('UsernameToken Username="%s", PasswordDigest="%s", Nonce="%s", Created="%s"', $this->ibanFirstLogin, $digest, $nonce64, $dateString);
        } catch (\Throwable $e) {
            $this->logger->error('Error during WSSE Header computing: '.$e->getMessage());
            $wsseHeader = '';
        }

        return $wsseHeader;
    }
}
