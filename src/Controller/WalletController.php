<?php

namespace App\Controller;

use App\Client\IbanFirstClient;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class WalletController extends AbstractController
{
    /**
     * @Route("/", name="wallet_list")
     */
    public function list(IbanFirstClient $ibanFirstClient)
    {
        $walletList = $ibanFirstClient->requestListWallets();

        return $this->render('home/wallet_list.html.twig', [
            'walletList' => $walletList['wallets'] ?? null,
        ]);
    }
}
