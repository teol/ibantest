<?php

namespace App\Controller;

use App\Client\IbanFirstClient;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MovementController extends AbstractController
{
    /**
     * @Route("/{walletId}/movements", name="movement_list")
     */
    public function list(string $walletId, IbanFirstClient $ibanFirstClient)
    {
        $movementList = $ibanFirstClient->requestWalletMovements($walletId);

        return $this->render('movement/movement_list.html.twig', [
            'movementList' => $movementList['financialMovements'] ?? null,
            'walletId'     => $walletId
        ]);
    }

    /**
     * @Route("/movements/{movementId}", name="movement_view")
     */
    public function view(string $movementId, IbanFirstClient $ibanFirstClient)
    {
        $movementDetails = $ibanFirstClient->requestMovementDetails($movementId);

        return $this->render('movement/movement_view.html.twig', [
            'movement' => $movementDetails['financialMovement'] ?? null,
        ]);
    }
}
