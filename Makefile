build:
	@docker-compose build --no-cache
	@make -s clean

start:
	@docker-compose up -d

stop:
	@docker-compose stop

restart:
	@make -s stop
	@make -s start

clean:
	@docker system prune --volumes --force

install:
	@docker-compose build --no-cache
	@docker-compose up -d
	@docker-compose exec php php /usr/local/bin/composer install

sh:
	@docker-compose exec php sh

cc:
	@docker-compose exec php php bin/console c:c

